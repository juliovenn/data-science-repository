import cx_Oracle
import sys
import os

try:

    if sys.platform.startswith("darwin"):
        lib_dir = os.path.join(os.environ.get("HOME"), "Documents",
                               "instantclient_19_8")
        cx_Oracle.init_oracle_client(lib_dir=lib_dir)

    elif sys.platform.startswith("win32"):
        lib_dir=r"C:\oracle\instantclient_19_9"
        cx_Oracle.init_oracle_client(lib_dir=lib_dir)

except Exception as err:
    print("Whoops!")
    print(err);
    sys.exit(1);
    
    
#PESDB_PDB.pesapexsubnet.vcn11241051.oraclevcn.com
dsn_tns = cx_Oracle.makedsn('vennii.info', '1521', service_name='PESDB_PDB.pesapexsubnet.vcn11241051.oraclevcn.com')
conn = cx_Oracle.connect(user=r'PES_DEV', password='PES_DEV', dsn=dsn_tns)